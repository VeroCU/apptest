import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppiServisesService } from '../appi-servises/appi-servises.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
	userLog:any;
 	return:Observable<any>;
 	constructor(
 	private router:Router,
 	private apiservice:AppiServisesService) {
	  	this.userLog = {
	  		email:"",
	  		password:""
	  	};
  	}

	ngOnInit() {
	}

	login(){
		this.apiservice.userLogin("appUsers",this.userLog,"login").subscribe(
	  	returned => {
	  		this.router.navigate(["/home"]);
	  	},
	  	error=>{
	  		console.log("error", error);
	  	}
		);
	}
	register(){
		console.log("registro");
		this.router.navigate(["/newuser"]);
	}
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AppiServisesService {
	url = 'http://localhost:3000/api/';
 
	constructor(private http: HttpClient) { 
	}
	/// model -- tabla donde se guarda; ejem. appUsers 
	/// credentials -- objeto a guardar -- user{'name': 'Carlos'}
	//  type -- el metodo (api) -- ejemplo : login
	userLogin(model: string, credentials: any, type: string) {
		let json = JSON.stringify(credentials);
        let parsejson = JSON.parse(json);

		let headers = new Headers({ 'Content-Type': 'application/json' });
		console.log(this.url + model + '/' + type + parsejson);
		return this.http.post(this.url + model + '/' + type, parsejson, { headers: httpOptions.headers }).pipe(map(res => res));
	}
	getCards(){
		return this.http.get("https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=76b2eb6e02364b55b5ca59397e9ce653").pipe(map(res => res));
	}

	save(model: string, credentials:any){
		let jsonCont = JSON.stringify(credentials);
		let params = jsonCont;
		return this.http.post(this.url + model, params, { headers: httpOptions.headers }).pipe(map(res => res));
	}

}

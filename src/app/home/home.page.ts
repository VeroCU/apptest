import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppiServisesService } from '../appi-servises/appi-servises.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
	listCards:any;
	constructor(
	 	private router:Router,
	 	private apiservice:AppiServisesService) {
	  	this.listCards={};
	  	this.getListCards();
	}

	getListCards(){
	  	this.apiservice.getCards().subscribe(
		  	listcards => {
		  		this.listCards=listcards;
		  	}
		);
	}
	newsId(id){
		console.log("amonos", id);
		this.router.navigate(["/detalle", id]);
	}

}

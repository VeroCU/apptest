import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AppiServisesService } from '../appi-servises/appi-servises.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage{
	item:any;
	cards:any;
	constructor(
	 	private router:Router,
	 	private apiservice:AppiServisesService,private route: ActivatedRoute) {
	  	this.item = this.route.snapshot.paramMap.get('id');
	  	this.cards={};
	  	this.getListCards();
	}

getListCards(){
  	this.apiservice.getCards().subscribe(
	  	listcards => {
	  		let listCards:any=listcards;
	  		this.cards= listCards.articles[this.item];
	  		console.log("algo",this.cards);
	  	}
	);
}


}

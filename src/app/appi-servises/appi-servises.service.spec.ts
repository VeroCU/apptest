import { TestBed } from '@angular/core/testing';

import { AppiServisesService } from './appi-servises.service';

describe('AppiServisesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppiServisesService = TestBed.get(AppiServisesService);
    expect(service).toBeTruthy();
  });
});

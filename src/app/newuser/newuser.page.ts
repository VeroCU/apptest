import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AppiServisesService } from '../appi-servises/appi-servises.service';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.page.html',
  styleUrls: ['./newuser.page.scss'],
})
export class NewuserPage implements OnInit {
	registerCredentials:any;
 	return:Observable<any>;
 	constructor(
 	private router:Router,
 	private apiservice:AppiServisesService) {
	  	this.registerCredentials = {
			firstName:"",
			lastName:"",
			email:"",
			password:""
	  	};
  	}
	ngOnInit() {}

  	newRegister(){
  	this.apiservice.save("appUsers",this.registerCredentials).subscribe(
	  	returned => {
	  		alert("Registro completado exitosamente, ingresa ahora");
	  		this.router.navigate(["/login"]);
	  	},
	  	error=>{
	  		console.log("error", error);
	  	}
		);
  	}

}
